#!/bin/bash
##################################################################################
# Shellscript : datausage.c.30m.sh                                       .--.    #
# Author      : Venkata Kolagotla <vkolagotla@pm.me>                    |ö_ö |   #
# Created     : 13-04-2021                                              |\ü/ |   #
# Last Updated: 26-12-2023                                             //   \ \  #
# Requires    : vnstat                                                (|     | ) #
# Category    : Argos plugin(GNOME desktop)                          /'\_   _/`\\#
# Version     : v0.1.3                                               \___)=(___//#
# License     : GNU GPLv3                                                        #
##################################################################################
# Description :  Realtime WLAN datausage for current day, updates every 30 minutes
#                You should install and initialize vnstat before using this script
#                $ sudo apt install vnstat
#                $ sudo systemctl enable vnstat.service
#                $ sudo systemctl start vnstat.service
# Usage       :  copy the script to ~/.config/argos/
##################################################################################

# get todays date and current month
today=$(date +"%Y-%m-%d")
yesterday=$(date -d "1 days ago" +"%Y-%m-%d")
month=$(date +"%Y-%m")
substring="|"

# get the data-usage from vnstat for today
datastats_today=$(vnstat --style 0 -d 1 | grep $today)
# format to get the final value
new_datastats=${datastats_today#*$substring}
today_datausage=${new_datastats#*$substring}
# print the final number to the console
echo ${today_datausage//"i"/}

# get the data-usage from vnstat for yesterday
datastats_yest=$(vnstat --style 0 -d 2 | grep $yesterday)
# format to get the final value
yest_datastats=${datastats_yest#*$substring}
yesterday_datausage=${yest_datastats#*$substring}

# print the daily usage number to the console
echo "---"
echo "Yesterday Usage"
if [ -z "$yesterday_datausage" ]; then
    echo "0 GB"
else
    # print the final number to the console
    echo ${yesterday_datausage//"i"/}
fi

# get the data-usage for this month
datastats_month=$(vnstat --style 0 -m 1 | grep $month)
# get the data-usage projection for this month
datastats_esti_month=$(vnstat --style 0 -m 1 | grep 'estimated')
# format to get the final value
m_new_datastats=${datastats_month#*$substring}
monthly_datausage=${m_new_datastats#*$substring}
# format to get the final value
m_esti_datastats=${datastats_esti_month#*$substring}
monthly_datausage_esti=${m_esti_datastats#*$substring}

# print the monthly numbers to the console
echo "---"
echo "Current Month Usage"
echo ${monthly_datausage//"i"/}
echo "---"
echo "Current Month Projection"
echo ${monthly_datausage_esti//"i"/}

# option to refresh the stats
echo "---"
echo "Refresh Stats | refresh=true"
