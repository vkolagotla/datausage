<p align="center"><img src="static/logo.png" width="160" height="160">
<h2 align="center">DataUsage</h2>

Simple GNOME Shell extension(Argos plugin) to show WLAN data usage(Daily and Monthly)

## How to Install and Use?

**Dependencies:**

* [Argos](https://github.com/Coda-Coda/argos.git) (recomended for all gnome versions) / [Argos](https://github.com/rammie/argos/tree/gnome-3.36) (gnome-3.36 branch for gnome versions <40)
* It needs `vnstat` installed and activated to start logging the data usage.

```bash
# install vnstat
sudo apt install vnstat
# enable and start vnstat service
systemctl enable vnstat.service
systemctl start vnstat.service
```

**Installation:**

Just clone this repo and copy the `datausage.c.30m.sh` script to `~/.config/argos/`

```bash
# clone the repo
git clone https://gitlab.com/vkolagotla/datausage.git
# copy the script to argos dir
cp datausage/datausage.c.30m.sh ~/.config/argos/ && chmod +x ~/.config/argos/datausage.c.30m.sh
```

That should do it. You should be able to see the stats on your GNOME panel. If you don't see it, try restarting the GNOME shell with `Alt+F2` and the type `r`

## Thanks to

* Contributers of [Argos](https://github.com/rammie/argos/tree/gnome-3.36)

* <div>Logo made using the work of <a href="https://www.flaticon.com/authors/pixel-perfect" title="Pixel perfect">Pixel perfect</a>, <a href="https://www.freepik.com" title="Freepik">Freepik</a> and <a href="https://www.flaticon.com/authors/eucalyp" title="Eucalyp">Eucalyp</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

[Go UP⬆️](#datausage)

